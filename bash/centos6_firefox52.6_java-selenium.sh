#!/bin/bash
# java 8
# centos 6
# firefox 52.6
# selenium 3.6 on jar 
# geckodriver 0.15.0

cd /tmp

wget --no-check-certificate --no-cookies - --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.rpm
rpm -ivh jdk-8

yum -y install firefox xorg-x11-server-Xvfb
sudo yum -y groupinstall "Japanese Support"

wget https://github.com/mozilla/geckodriver/releases/download/v0.15.0/geckodriver-v0.15.0-linux64.tar.gz
tar zxvf geckodriver-v0.15.0-linux64.tar.gz
chmod +x geckodriver
mv geckodriver /usr/local/bin/geckodriver
echo 'export PATH="/usr/local/bin/geckodriver:$PATH"' >> ~/.bash_profile
source ~/.bash_profile
sudo Xvfb :1 &
export DISPLAY=:1
dbus-uuidgen > /var/lib/dbus/machine-id

echo "SUCESS!!! "
echo "You do [ java -jar ./lib/sample_selenium_GetTitle.jar linux ]"
