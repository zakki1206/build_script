package build_script;

import static com.codeborne.selenide.Selenide.open;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;

import build_script.page.GooglePage;


public class SelenideTest {
	@Test
	public void test() throws Exception {
		String browser = "firefox";
		switch (browser) {
		case "firefox":
			Configuration.browser = WebDriverRunner.FIREFOX;
			if (PlatformUtils.isWindows()) {
//				System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver.exe");
			} else {
//				System.out.println("is LINUX!! ");
//				System.setProperty("webdriver.firefox.driver", "/usr/local/bin/geckodriver");
			}
			break;
		}
		
//		if (PlatformUtils.isWindows()) {
//			System.setProperty("webdriver.chrome.driver", "driver/chromedriver2.38_win.exe");
//		} else {
//			System.setProperty("webdriver.chrome.driver", "/var/lib/jenkins/driver/chromedriver2.38_linux64");
//		}
		System.out.println("---------------- start open");
		GooglePage googlepage = open("https://www.google.co.jp", GooglePage.class);
		System.out.println("start getTitle");
		String actualTitile = googlepage.getTitle();
		System.out.println("TITLE: " + actualTitile);
		assertThat(actualTitile, is("Google"));
		
	}

}

