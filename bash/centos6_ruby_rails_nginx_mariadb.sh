#!/bin/bash
#
# cd /tmp
# vi install.sh
# chmod +x install.sh
# ./install.sh
#

PW="gooo0501"
SERVER_NAME="160.16.101.171"
yum update
yum install -y expect

#=======  Ruby ======= 
cd /tmp

# rbenv
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
~/.rbenv/bin/rbenv init
source ~/.bash_profile

# ruby-build
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
sudo ~/.rbenv/plugins/ruby-build/install.sh

# install Ruby need liv
sudo yum install -y openssl-devel readline-devel zlib-devel 

# Ruby
rbenv install 2.4.0

# Add to ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
source ~/.bash_profile

# rbenv conf
rbenv global 2.4.0


#=======  Rails ======= 
# add repository
curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -

# Node.js 
yum install -y nodejs
yum install -y npm --enablerepo=epel

# SQLite3 
sudo yum install -y sqlite sqlite-devel 
# install rails need lib
yum install -y gcc-c++ git glibc-headers libffi-devel libxml2 libxml2-devel libxslt libxslt-devel libyaml-devel make nodejs npm openssl-devel readline readline-devel sqlite-devel zlib zlib-devel

yum install -y ruby-devel libffi-devel

# For mysql2 of gems
sudo yum install -y mysql-devel

sudo yum install -y gem

# SETTING [DON'T INSTALL DOCUMENT]
echo "gem: --no-ri --no-rdoc" > ~/.gemrc

# update gem
gem update --system

# UPDATE Ruby Library
# gem update
expect -c "
	set timeout 5
	spawn env LANG=C /root/.rbenv/shims/gem update
	expect \"Overwrite\"
	send \"y\n\"
	expect \"$\"
	exit 0
"

# SQLite3
gem install sqlite3
gem install rails -v 5.1.5

# m4 AND ffi
cd /usr/local/src
wget http://ftp.gnu.org/gnu/m4/m4-1.4.15.tar.gz
tar zxvf m4-1.4.15.tar.gz
cd m4-1.4.15
./configure --prefix=/usr
make
make install

cd /usr/local/src
wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.68.tar.gz
tar zxvf autoconf-2.68.tar.gz
cd autoconf-2.68
./configure --prefix=/usr
make
make install

cd /tmp
#=======  MariaDB ======= 

cat <<- EOT > /etc/yum.repos.d/MariaDB.repo
# MariaDB 5.5 CentOS repository list - created 2018-02-22 21:24 UTC
# http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/5.5/centos6-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOT
yum install -y MariaDB-devel MariaDB-client MariaDB-server

service mysql start
chkconfig mysql on 
#=======  Nginx ======= 

yum -y install epel-release
yum -y install nginx

# iptable 80port open
cp /etc/sysconfig/iptables /etc/sysconfig/iptables.backup
sed -e "/tcp/a -A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT" /etc/sysconfig/iptables.backup > /etc/sysconfig/iptables

service iptables start
chkconfig iptables on 


PW="gooo0501"
SERVER_NAME="160.16.101.171"

# nginx config
cat <<- EOF > /etc/nginx/conf.d/deploytest.conf

upstream deploytest {
        server 0.0.0.0:3000;
}
server {
        listen 80;
        server_name ${SERVER_NAME};
        root /var/www/deploytest/public;

        location / {
                try_files \$uri @deploytest;
        }
        
        location @deploytest {
                proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
                proxy_set_header Host \$http_host;
                proxy_pass http://deploytest;
        }
        error_page 500 502 503 504 /500.html;
}
EOF

service nginx start
chkconfig nginx on 

#======= CREATE BASH_FILE FOR SETTING Deploytest ======= 
#======= 

mkdir /var/www/

cat <<- EOT > /var/www/BUILD_APP.sh
#!/bin/bash

# rails app 
source ~/.bash_profile
cd /var/www

expect -c "
	set timeout 5
	spawn env LANG=C /usr/bin/git clone git@bitbucket.org:zakki1206/deploytest.git
	expect \"Enter passphrase for key '/root/.ssh/id_rsa':\"
	send \"${PW}\n\"
	expect \"$\"
	exit 0
"


cd /var/www/deploytest/ && bundle install
cd /var/www/deploytest/ && rails db:create RAILS_ENV=production
cd /var/www/deploytest/ && rails db:migrate RAILS_ENV=production
cd /var/www/deploytest/ && rails db:fixtures:load RAILS_ENV=production
cd /var/www/deploytest/ && rails assets:precompile RAILS_ENV=production


service  nginx restart
service  iptables restart

cat << EOF

SUCESS!!!!
YOU EXECUTE  >>> 

	export SECRET_KEY_BASE=\`cd /var/www/deploytest/ && rails secret\`  
	rails s -e production


EOF


EOT

chmod +x /var/www/BUILD_APP.sh

#======= COMMENT ▼▼▼ ======= 
cat << EOF

SUCESS!!!!


YOU EXECUTE  >>> [ source /var/www/BUILD_APP.sh  ]

EOF
#======= COMMENT ▲▲▲ ======= 



