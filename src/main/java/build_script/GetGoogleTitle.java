package build_script;

import java.nio.file.Paths;

import org.openqa.selenium.firefox.FirefoxDriver;

public class GetGoogleTitle {

	public static final String PROJ_DIR = System.getProperty("user.dir");
	public static final String SRC_DIR = Paths.get(PROJ_DIR, "src").toString();
	public static final String RESOUCES_DIR = Paths.get(SRC_DIR, "main", "resources").toString();

	public static void main(String[] args) {

		System.out.println("This program access google.co.jp and show Title");

		System.out.println("usage: java -jar ...jar linux");
		System.out.println("geckodriver path [/usr/local/bin/geckodriver]");

		String geckodriverPath = null;
		
		if (args.length > 0) {
			
//			geckodriverPath = "/usr/local/bin/geckodriver";
		} else {
			// must setting [webdriver.gecko.driver] on windows
			geckodriverPath = "/usr/local/bin/geckodriver.exe";
			System.setProperty("webdriver.gecko.driver", geckodriverPath);
		}
		FirefoxDriver driver = new FirefoxDriver();
		driver.get("https://www.google.co.jp");
		String title = driver.getTitle();
		System.out.println("You are SUCCESS !!!!");
		System.out.println("driver.getTitle() : " + title);
		System.out.println("driver.quit()");
		driver.quit();
	}



}
