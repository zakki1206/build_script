package build_script.page;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import org.openqa.selenium.By;

public class GooglePage {
	
	public String getTitle() {
		return $(By.tagName("title")).getText();
	}

}
